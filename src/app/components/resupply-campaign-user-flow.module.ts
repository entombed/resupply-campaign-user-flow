import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResupplyCampaignUserFlowRoutingModule } from './resupply-campaign-user-flow-routing.module';
import { ResupplyCampaignUserFlowComponent } from './resupply-campaign-user-flow.component';

@NgModule({
    declarations: [
        ResupplyCampaignUserFlowComponent,
    ],
    imports: [
        CommonModule,
        ResupplyCampaignUserFlowRoutingModule,
    ],
})
export class ResupplyCampaignUserFlowModule {
}
