/* tslint:disable:no-any */ // TODO: remove after create API

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { IAnswer } from '../../interfaces/answer.interface';
import { EYesNo } from '../../enums/yes-no.enum';
import { ISurvey } from '../../interfaces/survey.interface';
import { ResupplyCampaignUserFlowService } from '../../services/resupply-campaign-user-flow.service';
import { Observable, Subject } from 'rxjs';


@Component({
    selector: 'app-user-flow-yes-no-page',
    templateUrl: './user-flow-yes-no-page.component.html',
    styleUrls: ['./user-flow-yes-no-page.component.scss'],
})
export class UserFlowYesNoPageComponent implements OnInit, OnDestroy {
    public questionNumber$ = new Observable<number>();
    public yesNoPageData$ = new Observable<ISurvey>();
    public optionsAnswer = EYesNo;
    public answer!: string;
    private id!: string;
    private destroy$ = new Subject<void>();

    constructor(
        private activateRoute: ActivatedRoute,
        private resupplyCampaignUserFlowService: ResupplyCampaignUserFlowService,
    ) {
    }

    public ngOnInit(): void {
        this.initStreams();
        this.initSubscriptions();
    }

    public ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public answerSelected(answer: string): void {
        this.answer = answer;
        this.resupplyCampaignUserFlowService.updateSurveyAnswers({id: this.id, answer: this.answer});
    }

    private initStreams(): void {
        this.questionNumber$ = this.resupplyCampaignUserFlowService.getSurveyQuestionNumber();

        this.yesNoPageData$ = this.activateRoute.paramMap.pipe(
            map((data) => data.get('surveyId') as string),
            tap((id) => this.id = id),
            map((id: string) => this.resupplyCampaignUserFlowService.getSurveyPageDataById(id)),
            shareReplay(),
        );
    }

    private initSubscriptions(): void {
        this.yesNoPageData$.pipe(
            takeUntil(this.destroy$),
        ).subscribe({
            next: () => {
                const answer = this.resupplyCampaignUserFlowService.getStoredAnswer(this.id) as IAnswer<string>;
                this.answer = answer ? answer.answer : this.answer;
            },
        });
    }
}
