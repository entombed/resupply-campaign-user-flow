import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFlowYesNoPageComponent } from './user-flow-yes-no-page.component';
import { NavigationButtonsModule } from '../../modules/navigation-buttons/navigation-buttons.module';
import { UserFlowYesNoPageRoutingModule } from './user-flow-yes-no-page-routing.module';

@NgModule({
    declarations: [UserFlowYesNoPageComponent],
    imports: [
        CommonModule,
        NavigationButtonsModule,
        UserFlowYesNoPageRoutingModule
    ],
})
export class UserFlowYesNoPageModule {}
