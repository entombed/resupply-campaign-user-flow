import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserFlowYesNoPageComponent } from './user-flow-yes-no-page.component';

const routes: Routes = [
    {
        path: '',
        component: UserFlowYesNoPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserFlowYesNoPageRoutingModule {
}
