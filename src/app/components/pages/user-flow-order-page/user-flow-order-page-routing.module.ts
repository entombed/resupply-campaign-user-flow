import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserFlowOrderPageComponent } from './user-flow-order-page.component';

const routes: Routes = [
    {
        path: '',
        component: UserFlowOrderPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserFlowOrderPageRoutingModule {
}
