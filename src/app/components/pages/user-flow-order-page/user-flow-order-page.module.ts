import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwitchToggleModule } from '../../modules/switch-toggle/switch-toggle.module';
import { UserFlowOrderPageComponent } from './user-flow-order-page.component';
import { UserFlowOrderPageRoutingModule } from './user-flow-order-page-routing.module';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        UserFlowOrderPageComponent,
    ],
    imports: [
        CommonModule,
        SwitchToggleModule,
        MatDialogModule,
        UserFlowOrderPageRoutingModule,
        FormsModule,
    ],
    exports: [
        UserFlowOrderPageComponent,
    ],
})
export class UserFlowOrderPage {}
