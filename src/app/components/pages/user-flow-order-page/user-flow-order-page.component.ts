/* tslint:disable:no-any */ // TODO: remove after ready API

import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ResupplyCampaignUserFlowService } from '../../services/resupply-campaign-user-flow.service';
import { Router } from '@angular/router';
import { DeclineOrderComponent } from '../../modals/decline-order/decline-order.component';

@Component({
    selector: 'app-user-flow-order',
    templateUrl: './user-flow-order-page.component.html',
    styleUrls: ['./user-flow-order-page.component.scss'],
})
export class UserFlowOrderPageComponent implements OnInit, OnDestroy {

    public orderData!: any;
    private destroy$ = new Subject<void>();

    constructor(
        private dialog: MatDialog,
        private router: Router,
        private resupplyCampaignUserFlowService: ResupplyCampaignUserFlowService,
    ) {
    }

    public ngOnInit(): void {
        this.orderData = this.resupplyCampaignUserFlowService.orderPage;
    }

    public ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public confirm(): void {
        const route = this.resupplyCampaignUserFlowService.getNextSurveyPage(0);
        this.router.navigate(route);
    }

    public decline(): void {
        this.dialog.open(DeclineOrderComponent, {
            width: '480px',
        }).afterClosed().pipe(
            takeUntil(this.destroy$),
        ).subscribe();
    }

}
