/* tslint:disable:no-any */ // TODO: remove after create API

import { Component, OnDestroy, OnInit } from '@angular/core';
import { map, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { IAnswer } from '../../interfaces/answer.interface';
import { ISurvey } from '../../interfaces/survey.interface';
import { ResupplyCampaignUserFlowService } from '../../services/resupply-campaign-user-flow.service';


@Component({
    selector: 'app-user-flow-multiple-choice-page',
    templateUrl: './user-flow-multiple-choice-page.component.html',
    styleUrls: ['./user-flow-multiple-choice-page.component.scss'],
})
export class UserFlowMultipleChoicePageComponent implements OnInit, OnDestroy {
    public questionNumber$ = new Observable<number>();
    public answers: string[] = [];
    public multipleChoicePageData$ = new Observable<ISurvey>();
    private id!: string;
    private destroy$ = new Subject<void>();

    constructor(
        private activatedRoute: ActivatedRoute,
        private resupplyCampaignUserFlowService: ResupplyCampaignUserFlowService,
    ) {
    }

    public ngOnInit(): void {
        this.initStreams();
        this.initSubscriptions();
    }

    public ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public answerSelected(value: string): void {
        const exist = this.answers.includes(value);
        exist ? this.answers = this.answers.filter((item) => item !== value) : this.answers.push(value);
        this.resupplyCampaignUserFlowService.updateSurveyAnswers({id: this.id, answer: this.answers});
    }

    private initStreams(): void {
        this.questionNumber$ = this.resupplyCampaignUserFlowService.getSurveyQuestionNumber();

        this.multipleChoicePageData$ = this.activatedRoute.paramMap.pipe(
            map((data) => data.get('surveyId') as string),
            tap((id) => this.id = id),
            map((id: string) => this.resupplyCampaignUserFlowService.getSurveyPageDataById(id)),
            shareReplay(),
        );
    }

    private initSubscriptions(): void {
        this.multipleChoicePageData$.pipe(
            takeUntil(this.destroy$),
        ).subscribe({
            next: () => {
                const answer = this.resupplyCampaignUserFlowService.getStoredAnswer(this.id) as IAnswer<string[]>;
                this.answers = answer ? answer.answer : this.answers;
            },
        });
    }

}
