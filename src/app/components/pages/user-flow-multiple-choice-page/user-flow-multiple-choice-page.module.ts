import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { UserFlowMultipleChoicePageComponent } from './user-flow-multiple-choice-page.component';
import { UserFlowMultipleChoicePageRoutingModule } from './user-flow-multiple-choice-page-routing.module';
import { NavigationButtonsModule } from '../../modules/navigation-buttons/navigation-buttons.module';

@NgModule({
    declarations: [
        UserFlowMultipleChoicePageComponent,
    ],
    imports: [
        CommonModule,
        MatButtonModule,
        NavigationButtonsModule,
        UserFlowMultipleChoicePageRoutingModule,
    ],
})
export class UserFlowMultipleChoicePageModule {
}
