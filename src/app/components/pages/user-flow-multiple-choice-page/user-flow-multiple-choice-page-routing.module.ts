import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserFlowMultipleChoicePageComponent } from './user-flow-multiple-choice-page.component';

const routes: Routes = [
    {
        path: '',
        component: UserFlowMultipleChoicePageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserFlowMultipleChoicePageRoutingModule {
}
