/* tslint:disable:no-any */ // TODO: remove after ready API
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ResupplyCampaignUserFlowService } from '../../services/resupply-campaign-user-flow.service';

@Component({
    selector: 'app-user-flow-shipping-page',
    templateUrl: './user-flow-shipping-page.component.html',
    styleUrls: ['./user-flow-shipping-page.component.scss'],
})
export class UserFlowShippingPageComponent implements OnInit, OnDestroy {

    public form!: FormGroup;
    private destroy$ = new Subject<void>();

    constructor(
        private fb: FormBuilder,
        private resupplyCampaignUserFlowService: ResupplyCampaignUserFlowService,
        private router: Router,
    ) {
    }

    public ngOnInit(): void {
        this.initForm();
        this.initSubscriptions();
    }

    public ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public backToSurvey(): void {
        const url = this.resupplyCampaignUserFlowService.getNextSurveyPage(0);
        this.router.navigate(url);
    }

    private initForm(): void {
        this.form = this.fb.group({
            fullName: ['Some text', Validators.required],
            phone: [null, Validators.required],
            address1: ['Some text', Validators.required],
            address2: ['Some text'],
            city: ['Some text', Validators.required],
            state: ['State', Validators.required],
            zipCode: ['ZIP CODE', Validators.required],
        });
        const storedShippingAddress = this.resupplyCampaignUserFlowService.getStoredShippingAddress();
        if (storedShippingAddress) {
            this.form.patchValue(storedShippingAddress);
        }
    }

    private initSubscriptions(): void {
        this.form.valueChanges.pipe(
            debounceTime(500),
            takeUntil(this.destroy$),
        ).subscribe({
            next: (value: any) => this.resupplyCampaignUserFlowService.updateShippingAddress(value),
        });
    }

}
