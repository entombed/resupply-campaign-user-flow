import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { UserFlowShippingPageComponent } from './user-flow-shipping-page.component';
import { UserFlowShippingPageRoutingModule } from './user-flow-shipping-page-routing.module';

@NgModule({
    declarations: [
        UserFlowShippingPageComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        UserFlowShippingPageRoutingModule,
    ],
})
export class UserFlowShippingPageModule {}
