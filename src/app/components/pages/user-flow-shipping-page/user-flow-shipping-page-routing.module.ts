import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserFlowShippingPageComponent } from './user-flow-shipping-page.component';

const routes: Routes = [
    {
        path: '',
        component: UserFlowShippingPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserFlowShippingPageRoutingModule {}
