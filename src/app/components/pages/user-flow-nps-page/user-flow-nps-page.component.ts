/* tslint:disable:no-any */ // TODO: remove after create API

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, shareReplay, takeUntil } from 'rxjs/operators';
import { IAnswer } from '../../interfaces/answer.interface';
import { ISurvey } from '../../interfaces/survey.interface';
import { ResupplyCampaignUserFlowService } from '../../services/resupply-campaign-user-flow.service';
import { Observable, Subject } from 'rxjs';


@Component({
    selector: 'app-user-flow-nps-page',
    templateUrl: './user-flow-nps-page.component.html',
    styleUrls: ['./user-flow-nps-page.component.scss'],
})
export class UserFlowNpsPageComponent implements OnInit, OnDestroy {
    public questionNumber$ = new Observable<number>();
    public npsPageData$ = new Observable<ISurvey>();
    public answer!: number;
    public numberQuestions = Array(10)
        .fill(null)
        .map((_, index) => index + 1);
    private id!: string;
    private destroy$ = new Subject<void>();

    constructor(
        private activateRoute: ActivatedRoute,
        private resupplyCampaignUserFlowService: ResupplyCampaignUserFlowService,
    ) {
    }

    public ngOnInit(): void {
        this.initStreams();
        this.initSubscriptions();
    }

    public ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public answerSelected(answer: number): void {
        this.answer = answer;
        this.resupplyCampaignUserFlowService.updateSurveyAnswers({id: this.id, answer: this.answer});
    }

    private initStreams(): void {
        this.questionNumber$ = this.resupplyCampaignUserFlowService.getSurveyQuestionNumber();

        this.npsPageData$ = this.activateRoute.paramMap.pipe(
            map((data) => data.get('surveyId') as string),
            map((id: string) => this.resupplyCampaignUserFlowService.getSurveyPageDataById(id)),
            shareReplay(),
        );
    }

    private initSubscriptions(): void {
        this.npsPageData$.pipe(
            takeUntil(this.destroy$),
        ).subscribe({
            next: () => {
                const answer = this.resupplyCampaignUserFlowService.getStoredAnswer(this.id) as IAnswer<number>;
                this.answer = answer ? answer.answer : this.answer;
            },
        });
    }

}
