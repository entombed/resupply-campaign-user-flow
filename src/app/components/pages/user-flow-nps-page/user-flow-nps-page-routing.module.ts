import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserFlowNpsPageComponent } from './user-flow-nps-page.component';

const routes: Routes = [
    {
        path: '',
        component: UserFlowNpsPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserFlowNpsPageRoutingModule {
}
