import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFlowNpsPageComponent } from './user-flow-nps-page.component';
import { NavigationButtonsModule } from '../../modules/navigation-buttons/navigation-buttons.module';
import { UserFlowNpsPageRoutingModule } from './user-flow-nps-page-routing.module';

@NgModule({
    declarations: [UserFlowNpsPageComponent],
    imports: [
        CommonModule,
        NavigationButtonsModule,
        UserFlowNpsPageRoutingModule,
    ],
})
export class UserFlowNpsPageModule {}
