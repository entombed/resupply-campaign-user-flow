import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFlowShortTextPageComponent } from './user-flow-short-text-page.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NavigationButtonsModule } from '../../modules/navigation-buttons/navigation-buttons.module';
import { UserFlowShortTextPageRoutingModule } from './user-flow-short-text-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        UserFlowShortTextPageComponent,
    ],
    imports: [
        CommonModule,
        MatInputModule,
        MatFormFieldModule,
        NavigationButtonsModule,
        UserFlowShortTextPageRoutingModule,
        ReactiveFormsModule,
    ],
})
export class UserFlowShortTextPageModule {
}
