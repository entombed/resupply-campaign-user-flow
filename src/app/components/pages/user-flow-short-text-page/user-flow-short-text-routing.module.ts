import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserFlowShortTextPageComponent } from './user-flow-short-text-page.component';

const routes: Routes = [
    {
        path: '',
        component: UserFlowShortTextPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserFlowShortTextPageRoutingModule {}
