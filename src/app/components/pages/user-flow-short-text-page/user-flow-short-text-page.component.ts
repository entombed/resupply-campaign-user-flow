/* tslint:disable:no-any */ // TODO: remove after create API

import { Component, OnDestroy, OnInit } from '@angular/core';
import { debounceTime, map, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IAnswer } from '../../interfaces/answer.interface';
import { ISurvey } from '../../interfaces/survey.interface';
import { ResupplyCampaignUserFlowService } from '../../services/resupply-campaign-user-flow.service';


@Component({
    selector: 'app-user-flow-short-text',
    templateUrl: './user-flow-short-text-page.component.html',
    styleUrls: ['./user-flow-short-text-page.component.scss'],
})
export class UserFlowShortTextPageComponent implements OnInit, OnDestroy {

    public questionNumber$ = new Observable<number>();
    public form!: FormGroup;
    public shortTextPageData$ = new Observable<ISurvey>();
    private id!: string;
    private destroy$ = new Subject<void>();

    constructor(
        private activatedRoute: ActivatedRoute,
        private resupplyCampaignUserFlowService: ResupplyCampaignUserFlowService,
        private fb: FormBuilder,
    ) {
    }

    public ngOnInit(): void {
        this.initForm();
        this.initStreams();
        this.initSubscriptions();
    }

    public ngOnDestroy(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    private initForm(): void {
        this.form = this.fb.group({
            answer: ['', Validators.required],
        });
    }

    private initStreams(): void {
        this.questionNumber$ = this.resupplyCampaignUserFlowService.getSurveyQuestionNumber();

        this.shortTextPageData$ = this.activatedRoute.paramMap.pipe(
            map((data) => data.get('surveyId') as string),
            tap((id: string) => this.id = id),
            map((id: string) => this.resupplyCampaignUserFlowService.getSurveyPageDataById(id)),
            shareReplay(),
        );
    }

    private initSubscriptions(): void {
        this.shortTextPageData$.pipe(
            takeUntil(this.destroy$),
        ).subscribe({
            next: () => {
                const answer = this.resupplyCampaignUserFlowService.getStoredAnswer(this.id) as IAnswer<string>;
                if (answer) {
                    this.form.patchValue({answer: answer.answer});
                }
            },
        });

        this.form.valueChanges.pipe(
            debounceTime(500),
            takeUntil(this.destroy$),
        ).subscribe({
            next: (form) => this.resupplyCampaignUserFlowService.updateSurveyAnswers({id: this.id, answer: form.answer}),
        });
    }
}
