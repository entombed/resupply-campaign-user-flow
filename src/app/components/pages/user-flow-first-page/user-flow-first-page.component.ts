import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { responseFromServer } from '../../services/source.helper';
import { ResupplyCampaignUserFlowService } from '../../services/resupply-campaign-user-flow.service';
import { AuthService } from '../../services/auth.service';
@Component({
    selector: 'app-user-flow-first-page',
    templateUrl: './user-flow-first-page.component.html',
    styleUrls: ['./user-flow-first-page.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class UserFlowFirstPageComponent implements OnInit {
    public form!: FormGroup;

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private resupplyCampaignUserFlowService: ResupplyCampaignUserFlowService,
        private authResupplyCampaignUserFlowService: AuthService,
    ) {
    }

    public ngOnInit(): void {
        this.initForm();
    }

    public goToOrderPage(): void {
        this.authResupplyCampaignUserFlowService.updateIsAuthenticated(true);
        this.resupplyCampaignUserFlowService.parseResponse(responseFromServer());
        this.router.navigate(['user-flow', 'order-page']);
    }

    private initForm(): void {
        this.form = this.fb.group({
            lastName: ['', Validators.required],
            birthdate: ['', Validators.required],
        });
    }
}
