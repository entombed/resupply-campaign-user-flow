import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UserFlowFirstPageComponent } from './user-flow-first-page.component';

const routes: Routes = [
    {
        path: '',
        component: UserFlowFirstPageComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserFlowFirstPageRoutingModule {
}
