import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { UserFlowFirstPageComponent } from './user-flow-first-page.component';
import { MatInputModule } from '@angular/material/input';
import {
    UserFlowFirstPageRoutingModule,
} from './user-flow-first-page-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [UserFlowFirstPageComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatNativeDateModule,
        UserFlowFirstPageRoutingModule,
    ],
})
export class UserFlowFirstPageModule {}
