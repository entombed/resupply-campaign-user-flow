/* tslint:disable:no-any */ // TODO: remove after create API
import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { map } from 'rxjs/operators';

@Component({
    selector: 'app-resupply-campaign-user-flow',
    templateUrl: './resupply-campaign-user-flow.component.html',
    styleUrls: ['./resupply-campaign-user-flow.component.scss'],
})
export class ResupplyCampaignUserFlowComponent implements OnInit {

    public generalInfo$ = new Observable<any>();

    constructor(
        private sanitizer: DomSanitizer,
    ) {
    }

    public ngOnInit(): void {
        this.generalInfo$ = of({
            logoImg: 'assets/images/resupply-campaign-user-flow/default-logo.png',
            footer: 'Have questions?<br> Contact us at <a href="tel:310-437-2766">310-437-2766</a> or <a href="mailto:orders@abc.dme.com">orders@abc.dme.com</a>',
        }).pipe(
            map((value: any) => ({
                logoImg: this.sanitizer.bypassSecurityTrustUrl(value.logoImg),
                footer: this.sanitizer.bypassSecurityTrustHtml(value.footer),
                }),
            ),
        );
    }
}
