// TODO: mockup data, received from server
export function responseFromServer(): unknown {
    return {

        id: '2a390a1a-081f-4531-bc09-c2827ad029c5',
        name: 'another cloned template',
        status: {
            id: 'Draft',
            name: 'Draft',
        },
        footerText: 'footer',
        version: 1,
        pages: [
            {
                id: '20ef8010-8b89-4799-8fb9-fa493c39a644',
                pageType: {
                    id: 'Order',
                    name: 'Order',
                },
                pageName: 'Order',
                pageDescription: 'Resupply Order',
                alteredText1: 'Text1',
                alteredText2: 'Text2',
                alteredText3: 'Text3',
                alteredText4: 'Text4',
                pageOrderNumber: 1,
                items: [{
                    name: 'Airsense 11 Disposable Filters',
                    category: 'Resmed',
                    qty: '1',
                    selected: true,
                },
                    {
                        name: 'AirFit F30i',
                        category: 'Resmed',
                        qty: '2',
                        selected: true,
                    },
                ],
            },
            {
                id: '5d36afbb-689b-4e87-9406-2d78f6887b30',
                pageType: {
                    id: 'Survey',
                    name: 'Survey',
                },
                pageName: 'Question1',
                pageDescription: 'Survey Question1',
                surveyQuestionType: {
                    id: 'Nps',
                    name: 'NPS (1-10)',
                },
                surveyPhraseQuestion: 'How likely would you recommend our service to your friends?',
                pageOrderNumber: 2,
                surveyAnswerOptions: [
                    {
                        id: '7323e230-1440-44e9-b71e-784c4bf034fe',
                        answer: '1',
                        answerSortOrder: 1,
                    },
                    {
                        id: '3768c641-65ae-4310-9a44-1a355475213a',
                        answer: '2',
                        answerSortOrder: 2,
                    },
                    {
                        id: 'dc6596d0-940f-4eab-ae75-753784963862',
                        answer: '3',
                        answerSortOrder: 3,
                    },
                    {
                        id: '625fb5d9-9f16-4b7b-8fad-a4d9d9551c2f',
                        answer: '4',
                        answerSortOrder: 4,
                    },
                    {
                        id: '951bdd96-22e1-43a5-a600-4e6e551f14d8',
                        answer: '5',
                        answerSortOrder: 5,
                    },
                    {
                        id: 'dc6386ff-cffb-4e54-bb15-a004d5cf2f75',
                        answer: '6',
                        answerSortOrder: 6,
                    },
                    {
                        id: '29e6a601-9a5a-4328-88d0-9c728aa583a4',
                        answer: '7',
                        answerSortOrder: 7,
                    },
                    {
                        id: 'c163084e-d7a7-4aac-b065-1dcb063b4edf',
                        answer: '8',
                        answerSortOrder: 8,
                    },
                    {
                        id: '9247d158-d23c-4739-a3e5-7225d81df073',
                        answer: '9',
                        answerSortOrder: 9,
                    },
                    {
                        id: 'b149334a-595f-4a97-a674-16eefa96e2d5',
                        answer: '10',
                        answerSortOrder: 10,
                    },
                ],
            },
            {
                id: '6151274e-fb70-4302-b673-9d7f724b7c63',
                pageType: {
                    id: 'Survey',
                    name: 'Survey',
                },
                pageName: 'Question2',
                pageDescription: 'Survey Question2',
                surveyQuestionType: {
                    id: 'YesNo',
                    name: 'Yes/No',
                },
                surveyPhraseQuestion: 'Have you been using your equipment as prescribed on a regular basis?',
                pageOrderNumber: 3,
                surveyAnswerOptions: [
                    {
                        id: '613958d6-66a4-4692-a223-24832d41a273',
                        answer: 'Yes',
                        answerSortOrder: 1,
                    },
                    {
                        id: '74e3de74-500c-4b9b-8bd4-f61956c5f3d1',
                        answer: 'No',
                        answerSortOrder: 2,
                    },
                ],
            },
            {
                id: 'fcd7a648-b68e-4ce8-b463-f640603eab2d',
                pageType: {
                    id: 'Survey',
                    name: 'Survey',
                },
                pageName: 'Question3',
                pageDescription: 'Survey Question3',
                surveyQuestionType: {
                    id: 'MultiSelect',
                    name: 'Multiple choice',
                },
                surveyPhraseQuestion: 'What’s your favourite product?',
                pageOrderNumber: 4,
                surveyAnswerOptions: [
                    {
                        id: '7adbe344-f200-4f20-b915-a18b2da02d14',
                        answer: 'Option1',
                        answerSortOrder: 1,
                    },
                    {
                        id: 'e0db0a8b-07c4-4bd1-b89f-622b5a553f68',
                        answer: 'Option2',
                        answerSortOrder: 2,
                    },
                    {
                        id: '40fe6a3a-523f-4931-84ea-c94e470bbc85',
                        answer: 'Option3',
                        answerSortOrder: 3,
                    },
                    {
                        id: '4fe708f4-c3b0-4503-8a58-16aa0e090370',
                        answer: 'Option4',
                        answerSortOrder: 3,
                    },
                ],
            },
            {
                id: 'bb6a2be3-b813-424d-bf69-eacc427bf89b',
                pageType: {
                    id: 'Survey',
                    name: 'Survey',
                },
                pageName: 'Question4',
                pageDescription: 'Survey Question4',
                surveyQuestionType: {
                    id: 'ShortText',
                    name: 'Short text',
                },
                surveyPhraseQuestion: 'How would you describe our product?',
                pageOrderNumber: 5,
            },
            {
                id: '385fe6ba-f4f7-4df3-8a1c-a028f9253a52',
                pageType: {
                    id: 'ShippingAddress',
                    name: 'ShippingAddress',
                },
                pageName: 'Shipping Address',
                pageDescription: 'Shipping Address Page',
                pageOrderNumber: 6,
            },
        ],
        created: {
            name: {
                first: 'Andrei',
                last: 'Saperski',
            },
            date: '2023-01-30T09:38:19.356077+00:00',
        },
    };

}
