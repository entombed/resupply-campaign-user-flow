/* tslint:disable:no-any */ // TODO: remove after create API

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import { IAnswer } from '../interfaces/answer.interface';
import { EPageType } from '../enums/page-type.enum';
import { ISurvey } from '../interfaces/survey.interface';

@Injectable({
    providedIn: 'root',
})
export class ResupplyCampaignUserFlowService {

    public orderPage!: any;
    public surveyId$ = new BehaviorSubject<number>(0);
    private surveyPages: ISurvey[] = [];
    private surveyAnswers: IAnswer<string | number | string[]>[] = [];
    private shippingAddress!: any;

    public getSurveyQuestionNumber(): Observable<number> {
        return this.surveyId$.pipe(
            map((id: number) => id + 1),
        );
    }

    public parseResponse(data: any): void {
        this.orderPage = data.pages.find((item: any) => item.pageName === EPageType.Order);
        this.surveyPages = data.pages.filter((item: any) => item.pageType.id === EPageType.Survey);
    }

    public getNextSurveyPage(page: number): string[] {
        let route: string[] = [];
        const index = this.surveyId$.value + page;
        if (index <= this.surveyPages.length - 1) {
            const survey = this.surveyPages[index] as ISurvey;
            const id = survey.id;
            const type = survey.surveyQuestionType.id.toLowerCase();
            route = ['user-flow', `${type}`, `${id}`];
            this.surveyId$.next(index);
        } else {
            route = ['user-flow', 'shipping-page'];
        }

        return route;
    }

    public getSurveyPageDataById(id: string): any {
        return this.surveyPages.find((item) => item.id === id);
    }

    public updateSurveyAnswers(answer: any): void {
        const index = this.surveyAnswers.findIndex((item) => item.id === answer.id);
        if (index !== -1) {
            this.surveyAnswers.splice(index, 1, answer);
        } else {
            this.surveyAnswers.push(answer);
        }
    }

    public getStoredAnswer(id: string): IAnswer<string | number | string[]> | undefined {
        return this.surveyAnswers.find((item: any) => item.id === id);
    }

    public updateShippingAddress(value: any): void {
        this.shippingAddress = {...value};
    }

    public getStoredShippingAddress(): any {
        return {...this.shippingAddress};
    }
}
