import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    private isAuthenticated$ = new BehaviorSubject<boolean>(false);

    public getIsAuthenticated(): Observable<boolean> {
        return this.isAuthenticated$;
    }
    public updateIsAuthenticated(value: boolean): void {
        this.isAuthenticated$.next(value);
    }
}
