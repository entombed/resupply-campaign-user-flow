import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-decline-order',
    templateUrl: './decline-order.component.html',
    styleUrls: ['./decline-order.component.scss'],
})
export class DeclineOrderComponent {

    constructor(
        private dialogRef: MatDialogRef<DeclineOrderComponent>,
    ) {
    }

    public close(value: boolean): void {
        this.dialogRef.close(value);
    }
}
