import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeclineOrderComponent } from './decline-order.component';

@NgModule({
    declarations: [
        DeclineOrderComponent,
    ],
    imports: [
        CommonModule,
    ],
})
export class DeclineOrderModule {
}
