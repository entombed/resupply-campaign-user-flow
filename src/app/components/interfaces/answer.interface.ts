export interface IAnswer<T> {
    id: string;
    answer: T;
}
