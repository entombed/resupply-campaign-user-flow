// tslint:disable:no-any

export interface ISurvey {
    id: string;
    pageType: IType;
    surveyQuestionType: IType;
    surveyPhraseQuestion: string;
    surveyAnswerOptions?: IAnswerOption[];
    [key: string]: any;
}

interface IType {
    id: string;
    name: string;
}

interface IAnswerOption {
    id: string;
    answer: string;
    answerSortOrder: number;
}
