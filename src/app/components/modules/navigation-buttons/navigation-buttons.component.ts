import { Component, OnInit } from '@angular/core';
import { ResupplyCampaignUserFlowService } from '../../services/resupply-campaign-user-flow.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ENavigation } from '../../enums/navigation.enum';

@Component({
    selector: 'app-navigation-buttons',
    templateUrl: './navigation-buttons.component.html',
    styleUrls: ['./navigation-buttons.component.scss'],
})
export class NavigationButtonsComponent implements OnInit {
    public isVisiblePreviousBtn$ = new Observable<boolean>();
    public navigation = ENavigation;

    constructor(
        private resupplyCampaignUserFlowService: ResupplyCampaignUserFlowService,
        private router: Router,
    ) {
    }

    public ngOnInit(): void {
        this.isVisiblePreviousBtn$ = this.resupplyCampaignUserFlowService.surveyId$.pipe(
            map((pageNumber: number) => Boolean(pageNumber)),
        );
    }

    public changePage(action: string): void {
        let route: string[] = [];
        switch (action) {
            case this.navigation.Next:
                route = this.resupplyCampaignUserFlowService.getNextSurveyPage(1);
                break;

            case this.navigation.Previous:
                route = this.resupplyCampaignUserFlowService.getNextSurveyPage(-1);
                break;

            default:
                break;
        }
        if (!route.length) {
            return;
        }
        this.router.navigate(route);
    }

}
