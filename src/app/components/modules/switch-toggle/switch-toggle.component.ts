import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-switch-toggle',
    templateUrl: './switch-toggle.component.html',
    styleUrls: ['./switch-toggle.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SwitchToggleComponent),
            multi: true,
        },
    ],
})
export class SwitchToggleComponent implements ControlValueAccessor {

    @Input() public set checked(value: boolean) {
        this.value = value;
    }

    @Input() public disabled = false;
    @Output() public changed = new EventEmitter<boolean>();

    public get value(): boolean {
        return this._toggleStatus;
    }

    public set value(value: boolean) {
        this._toggleStatus = value;
        this.onChange(value);
        this.onTouched();
    }

    private _toggleStatus = false;

    public registerOnChange(fn: (value: boolean | null) => void): void {
        this.onChange = fn;
    }

    public writeValue(value: boolean): void {
        if (value) {
            this.value = value;
        }
    }

    public registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    public toggle(): void {
        if (this.disabled) {
            return;
        }
        this.value = !this.value;
    }

    private onChange(value: boolean): void {

        this.changed.emit(value);
    }
    // tslint:disable-next-line:no-empty
    private onTouched: () => void = () => {};
}
