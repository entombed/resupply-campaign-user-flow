import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResupplyCampaignUserFlowComponent } from './resupply-campaign-user-flow.component';
import { AuthResupplyCampaignUserFlowGuard } from './guards/auth-resupply-campaign-user-flow-guard.service';

const routes: Routes = [
    {
        path: '',
        component: ResupplyCampaignUserFlowComponent,
        children: [
            {
                path: '',
                redirectTo: '/user-flow/first-page',
                pathMatch: 'full',
            },
            {
                path: 'first-page',
                loadChildren: () => import('./pages/user-flow-first-page/user-flow-first-page.module')
                    .then((m) => m.UserFlowFirstPageModule),
            },
            {
                path: '',
                canActivateChild: [AuthResupplyCampaignUserFlowGuard],
                children: [
                    {
                        path: 'order-page',
                        loadChildren: () => import('./pages/user-flow-order-page/user-flow-order-page.module')
                            .then((m) => m.UserFlowOrderPage),
                    },
                    {
                        path: 'nps/:surveyId',
                        loadChildren: () => import('./pages/user-flow-nps-page/user-flow-nps-page.module')
                            .then((m) => m.UserFlowNpsPageModule),
                    },
                    {
                        path: 'yesno/:surveyId',
                        loadChildren: () => import('./pages/user-flow-yes-no-page/user-flow-yes-no-page.module')
                            .then((m) => m.UserFlowYesNoPageModule),
                    },
                    {
                        path: 'multiselect/:surveyId',
                        loadChildren: () => import('./pages/user-flow-multiple-choice-page/user-flow-multiple-choice-page.module')
                            .then((m) => m.UserFlowMultipleChoicePageModule),
                    },
                    {
                        path: 'shorttext/:surveyId',
                        loadChildren: () => import('./pages/user-flow-short-text-page/user-flow-short-text-page.module')
                            .then((m) => m.UserFlowShortTextPageModule),
                    },
                    {
                        path: 'shipping-page',
                        loadChildren: () => import('./pages/user-flow-shipping-page/user-flow-shipping-page.module')
                            .then((m) => m.UserFlowShippingPageModule),
                    },
                ],
            },
        ],
    },
    {
        path: '**',
        redirectTo: '/user-flow/first-page',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ResupplyCampaignUserFlowRoutingModule {
}
