import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable, tap } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class AuthResupplyCampaignUserFlowGuard implements CanActivate, CanActivateChild {
    constructor(
        private authResupplyCampaignUserFlowService: AuthService,
        private router: Router,
    ) {
    }

    public canActivate(): Observable<boolean> {
        return this.authResupplyCampaignUserFlowService.getIsAuthenticated().pipe(
            tap((value: boolean) => {
                if (!value) {
                    this.router.navigate(['/user-flow']);
                }
            }),
        );
    }

    public canActivateChild(): Observable<boolean> {
        return this.canActivate();
    }

}
