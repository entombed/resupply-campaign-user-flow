import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
        path: 'user-flow',
        loadChildren: () => import('./components/resupply-campaign-user-flow.module').then((m) => m.ResupplyCampaignUserFlowModule),
    },
    {
        path: '',
        redirectTo: '/user-flow/first-page',
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
